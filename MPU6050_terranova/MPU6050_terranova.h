/*
 * MPU6050_trn.h
 *
 *  Created on: Sep 6, 2021
 *      Author: jopj
 */

#ifndef MPU6050_TRN_H_
#define MPU6050_TRN_H_

#include "stdint.h"
#include "main.h"
#include "MPU6050_regmap.h"

class MPU6050_terranova{
public:
	MPU6050_terranova(uint8_t id, I2C_HandleTypeDef* I2C);
	bool ClockSource(uint8_t);
	bool SampleRate(uint8_t);
	bool DLPF(uint8_t);
	bool Fsync(uint8_t);
	bool FsSel(uint8_t);
	bool AfsSel(uint8_t);
	uint8_t Initialize();
	uint32_t read();
	void process();

	int loops;
	int16_t ax;
	int16_t ay;
	int16_t az;
	int16_t t;
	int16_t gx;
	int16_t gy;
	int16_t gz;

	volatile uint8_t _dataBuffer[14];

private:
	I2C_HandleTypeDef* _I2C;  //handle to the i2c peripheral
	uint16_t _id; //i2c address of the sensor
	uint8_t _clockSource;
	uint8_t _sampleRate;
	uint8_t _DLPF;
	uint8_t _Fsync;
	uint8_t _fsSel; //gyro full scale select
	uint8_t _afsSel; //accel full scale select

};


#endif /* MPU6050_TRN_H_ */
