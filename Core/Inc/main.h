/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define USB_PULL_D_Pin GPIO_PIN_13
#define USB_PULL_D_GPIO_Port GPIOC
#define FSYNC_Pin GPIO_PIN_14
#define FSYNC_GPIO_Port GPIOC
#define MPUINT_Pin GPIO_PIN_15
#define MPUINT_GPIO_Port GPIOC
#define MPUINT_EXTI_IRQn EXTI15_10_IRQn
#define CS2_Pin GPIO_PIN_1
#define CS2_GPIO_Port GPIOA
#define DRDY_Pin GPIO_PIN_4
#define DRDY_GPIO_Port GPIOA
#define BMPINT_Pin GPIO_PIN_5
#define BMPINT_GPIO_Port GPIOA
#define CS_Pin GPIO_PIN_6
#define CS_GPIO_Port GPIOA
#define WSDAT_Pin GPIO_PIN_7
#define WSDAT_GPIO_Port GPIOA
#define OUT1_Pin GPIO_PIN_0
#define OUT1_GPIO_Port GPIOB
#define OUT2_Pin GPIO_PIN_1
#define OUT2_GPIO_Port GPIOB
#define OUT3_Pin GPIO_PIN_2
#define OUT3_GPIO_Port GPIOB
#define OUT4_Pin GPIO_PIN_12
#define OUT4_GPIO_Port GPIOB
#define VBUS_D_Pin GPIO_PIN_8
#define VBUS_D_GPIO_Port GPIOA
#define OUT5_Pin GPIO_PIN_3
#define OUT5_GPIO_Port GPIOB
#define OUT9_Pin GPIO_PIN_4
#define OUT9_GPIO_Port GPIOB
#define OUT8_Pin GPIO_PIN_5
#define OUT8_GPIO_Port GPIOB
#define OUT10_Pin GPIO_PIN_6
#define OUT10_GPIO_Port GPIOB
#define OUT7_Pin GPIO_PIN_7
#define OUT7_GPIO_Port GPIOB
#define OUT6_Pin GPIO_PIN_8
#define OUT6_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
