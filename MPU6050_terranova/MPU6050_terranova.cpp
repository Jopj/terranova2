/*
 * MPU6050_trn.cpp
 *
 *  Created on: Sep 6, 2021
 *      Author: jopj
 */
#include "MPU6050_terranova.h"


MPU6050_terranova::MPU6050_terranova(uint8_t id, I2C_HandleTypeDef* I2C)
: loops{0}
, ax{0}
, ay{0}
, az{0}
, t{0}
, gx{0}
, gy{0}
, gz{0}
, _dataBuffer{0}
, _I2C{I2C}
, _id{id}
, _clockSource{0}
, _sampleRate{0}
, _DLPF{1}
, _Fsync{0}
, _fsSel{3}
, _afsSel{2}
{ }

bool MPU6050_terranova::ClockSource(uint8_t cs){
	_clockSource = cs;
	return true;
}

bool MPU6050_terranova::SampleRate(uint8_t sampleRate){
	_sampleRate = sampleRate;
	return true;
}

bool MPU6050_terranova::DLPF(uint8_t DLPF){
	_DLPF = DLPF;
	return true;
}

bool MPU6050_terranova::Fsync(uint8_t Fsync){
	_Fsync = Fsync;
	return true;
}

bool MPU6050_terranova::FsSel(uint8_t fsSel){
	_fsSel = fsSel;
	return true;
}

bool MPU6050_terranova::AfsSel(uint8_t afsSel){
	_afsSel = afsSel;
	return true;
}

uint8_t MPU6050_terranova::Initialize(){
	uint8_t whoami = 0;
	uint8_t ragester = 0;

	while(whoami != 0x68){  //If it matches, good
		HAL_Delay(20);
		HAL_I2C_Mem_Read(_I2C, _id<<1, WHO_AM_I_MPU6050, 1, &whoami, 1, 200); //Get contents of the sensor ID register
		loops++;
	}

	//Reset MPU
	ragester = 0x80;
	HAL_I2C_Mem_Write(_I2C, _id<<1, PWR_MGMT_1, 1 ,&ragester, 1, 200);

	//Make sure it is responding again
	whoami = 0;
	while(whoami != 0x68){  //If it matches, good
		HAL_Delay(20);
		HAL_I2C_Mem_Read(_I2C, _id<<1, WHO_AM_I_MPU6050, 1, &whoami, 1, 200); //Get contents of the sensor ID register
		loops++;
	}

	//take out of sleep
	ragester = 0x01;
	HAL_I2C_Mem_Write(_I2C, _id<<1, PWR_MGMT_1, 1 ,&ragester, 1, 200);
	HAL_Delay(200);

	//Set DLPF and sync input
	ragester = _DLPF | _Fsync <<3;
	HAL_I2C_Mem_Write(_I2C, _id<<1, CONFIG, 1, &ragester, 1, 200);

	//Set sample rate
	ragester = _sampleRate;
	HAL_I2C_Mem_Write(_I2C, _id<<1, SMPLRT_DIV, 1, &ragester, 1, 200);

	//Set gyro full scale range
	ragester = _fsSel<<3;
	HAL_I2C_Mem_Write(_I2C, _id<<1, GYRO_CONFIG, 1, &ragester, 1, 200);

	//Set accel full scale range
	ragester = _afsSel<<3;
	HAL_I2C_Mem_Write(_I2C, _id<<1, ACCEL_CONFIG, 1, &ragester, 1, 200);


	//Configure int to be cleared by any write
	ragester = 0x10;
	HAL_I2C_Mem_Write(_I2C, _id<<1, INT_PIN_CFG, 1, &ragester, 1, 200);

	//Enable data ready interrupt
	ragester = 0x01;
	HAL_I2C_Mem_Write(_I2C, _id<<1, INT_ENABLE, 1, &ragester, 1, 200);


	return whoami;
}

uint32_t MPU6050_terranova::read(){
	//uint8_t ekses[14] = {0};
	//HAL_I2C_Mem_Read(_I2C, _id<<1, ACCEL_XOUT_H, 1, ekses, 14, 200); //Get contents of the sensor ID register
	HAL_StatusTypeDef res = HAL_I2C_Mem_Read_DMA(_I2C, _id<<1, ACCEL_XOUT_H, 1, (uint8_t*)_dataBuffer, 14);



	return (uint16_t)res;
//	uint8_t ekses = 3;
//	HAL_StatusTypeDef stat;
//	stat = HAL_I2C_Mem_Read(_I2C, _id<<1, GYRO_XOUT_H, 1, &ekses, 1, 200); //Get contents of the sensor ID register
//	return (uint16_t) ekses;
	//return (int16_t)stat;
}

void MPU6050_terranova::process(){
	ax = (int16_t)((_dataBuffer[0] << 8) | _dataBuffer[1]);
	ay = (int16_t)((_dataBuffer[2] << 8) | _dataBuffer[3]);
	az = (int16_t)((_dataBuffer[4] << 8) | _dataBuffer[5]);

	t = (int16_t)((_dataBuffer[6] << 8) | _dataBuffer[7]);

	gx = (int16_t)((_dataBuffer[8] << 8) | _dataBuffer[9]);
	gy = (int16_t)((_dataBuffer[10] << 8) | _dataBuffer[11]);
	gz = (int16_t)((_dataBuffer[12] << 8) | _dataBuffer[13]);
}
